package service

import (
	"gitlab.com/moon1139/go_gingonic/entity"
	"gitlab.com/moon1139/go_gingonic/repository"
)

//ProviderService warning shutup
type ProviderService interface {
	Save(entity.Provider) error
	Update(entity.Provider) error
	Delete(entity.Provider) error
	FindAll() []entity.Provider
}

type providerService struct {
	repository repository.ProviderRepository
}

//New new a service
func New(providerRepository repository.ProviderRepository) ProviderService {
	return &providerService{
		repository: providerRepository,
	}
}

func (service *providerService) Save(provider entity.Provider) error {
	service.repository.Save(provider)
	return nil
}

func (service *providerService) Update(provider entity.Provider) error {
	service.repository.Update(provider)
	return nil
}

func (service *providerService) Delete(provider entity.Provider) error {
	service.repository.Delete(provider)
	return nil
}

func (service *providerService) FindAll() []entity.Provider {
	return service.repository.FindAll()
}
