package repository

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite" //warning shutup
	"gitlab.com/moon1139/go_gingonic/entity"
)

//ProviderRepository shutup warning
type ProviderRepository interface {
	Save(Provider entity.Provider)
	Update(Provider entity.Provider)
	Delete(Provider entity.Provider)
	FindAll() []entity.Provider
	CloseDB()
}

type database struct {
	connection *gorm.DB
}

//NewProviderRepository shutup warning
func NewProviderRepository() ProviderRepository {
	db, err := gorm.Open("sqlite3", "test.db")
	if err != nil {
		panic("Failed to connect database")
	}
	db.AutoMigrate(&entity.Provider{})
	return &database{
		connection: db,
	}
}

func (db *database) CloseDB() {
	err := db.connection.Close()
	if err != nil {
		panic("Failed to close database")
	}
}

func (db *database) Save(provider entity.Provider) {
	db.connection.Create(&provider)
}

func (db *database) Update(provider entity.Provider) {
	db.connection.Save(&provider)
}

func (db *database) Delete(provider entity.Provider) {
	db.connection.Delete(&provider)
}

func (db *database) FindAll() []entity.Provider {
	var providers []entity.Provider
	db.connection.Set("gorm:auto_preload", true).Find(&providers)
	return providers
}
