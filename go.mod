module gitlab.com/moon1139/go_gingonic

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.15
	github.com/stretchr/testify v1.4.0
	gitlab.com/pragmaticreviews/golang-gin-poc v0.0.0-20200217232121-d0a67e33c0bd
	gopkg.in/go-playground/validator.v9 v9.29.1
)
