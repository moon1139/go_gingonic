package controller

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/moon1139/go_gingonic/entity"
	"gitlab.com/moon1139/go_gingonic/service"
	"gopkg.in/go-playground/validator.v9"
)

//ProviderController bla
type ProviderController interface {
	FindAll() []entity.Provider
	Save(ctx *gin.Context) error
	Update(ctx *gin.Context) error
	Delete(ctx *gin.Context) error
	// ShowAll(ctx *gin.Context)
}

type controller struct {
	service service.ProviderService
}

var validate *validator.Validate

//New bla
func New(service service.ProviderService) ProviderController {
	validate = validator.New()
	return &controller{
		service: service,
	}
}

func (c *controller) FindAll() []entity.Provider {
	return c.service.FindAll()
}

func (c *controller) Save(ctx *gin.Context) error {
	var provider entity.Provider
	err := ctx.ShouldBindJSON(&provider)
	if err != nil {
		return err
	}
	err = validate.Struct(provider)
	if err != nil {
		return err
	}
	c.service.Save(provider)
	return nil
}

func (c *controller) Update(ctx *gin.Context) error {
	var provider entity.Provider
	err := ctx.ShouldBindJSON(&provider)
	if err != nil {
		return err
	}

	id, err := strconv.ParseUint(ctx.Param("id"), 0, 0)
	if err != nil {
		return err
	}
	provider.ID = id

	err = validate.Struct(provider)
	if err != nil {
		return err
	}
	c.service.Update(provider)
	return nil
}

func (c *controller) Delete(ctx *gin.Context) error {
	var provider entity.Provider
	id, err := strconv.ParseUint(ctx.Param("id"), 0, 0)
	if err != nil {
		return err
	}
	provider.ID = id
	c.service.Delete(provider)
	return nil
}

// func (c *controller) ShowAll(ctx *gin.Context) {
// 	providers := c.service.FindAll()
// 	data := gin.H{
// 		"title":  "Provider List",
// 		"providers": providers,
// 	}
// 	ctx.HTML(http.StatusOK, "index.html", data)
// }
